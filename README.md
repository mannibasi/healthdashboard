# README #

### OVERVIEW ###
Our firm has developed a series of web services which retrieve and integrate data from two sources – Strava (www.strava.com) and MyFitnessPal (www.myfitnesspal.com).
We are inviting proposals to develop a dashboard to show a cockpit type view of activity, nutrition and health information. The prototype dashboard is to be web-based with the ability to render on a mobile device browser.
As we develop our web services and bring together data from further third parties, the dashboard is expected to be expanded to incorporate this.  Multiple mobile device form factors will also need to be supported in due course.

### OBJECTIVES & REQUIREMENTS ###
# Objectives #
* Demonstrate to stakeholders how a dashboard which brings together data from multiple data sources could be used to create a mash-up
* Build confidence, internally within the firm, to leverage new models like Freelancer/Guru as part of its resource mix

# Requirements #
# Data Requirements #
The health dashboard prototype will display activity and nutritional information which has been gathered from 2 data sources:

# Strava (www.strava.com), activity information: #
* Activity Type - Run, Ride, Swim and ‘Other’ 
* Activity Details - Duration, Distance, Calories burnt, Elevation Gained (where available)
* Heart Rate information, where available

# MyFitnessPal (www.myfitnesspal.com), nutrition information: #
* Calorie Intake by Meal - Breakfast, Lunch, Dinner & Snacks
* Total Calories for the Day
* Nutrients – Protein, Carbohydrates, Fibre, Sugar, Fat 

The prototype should display weight, BMI and sleep-tracking information, alongside the other data retrieved/made available.

# Display Requirements #
* The prototype should allow users to scroll to view other days (backward and forward in time).  3 distinct pages of data are required as a minimum, but the values can be arbitrarily different.
* Integration with our web services is out of scope for the purposes of this initial RFP, however, the end goal will be to integrate the dashboard with our web service(s) which will provide the integrated data in a JSON format via a RESTful API.
* Given that the web services will provide the data being rendered in JSON format, the information displayed on the prototype dashboard should be read from a JSON object as opposed to being hard-coded within the HTML.  The JSON itself can be hosted locally if required.   
* Whilst making the prototype ‘live’ and accessible to the public is not required at this stage, we must be able to run the ‘web application’ on a local server and navigate through the pages/data available